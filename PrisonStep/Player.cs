using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace PrisonStep
{
    /// <summary>
    /// This class describes our player in the game. 
    /// </summary>
    public class Player
    {
        #region Fields

        /// <summary>
        /// This is a range from the door center that is considered
        /// to be under the door.  This much either side.
        /// </summary>
        private const float DoorUnderRange = 40;

        /// <summary>
        /// how many seconds should be between each spit
        /// </summary>
        private const float SpitFrequency = 2f;

        /// <summary>
        /// how many seconds between each spit
        /// </summary>
        private double SpitTime = 0;

        /// <summary>
        /// Game that uses this player
        /// </summary>
        private PrisonGame game;

        /// <summary>
        /// Our animated model
        /// </summary>
        private AnimatedModel victoria;

        private float alienSpitVelocity = 6f;

        /// <summary>
        /// the bazooka
        /// </summary>
        //private Bazooka bazooka;

        /// <summary>
        /// the pies
        /// </summary>
        private LinkedList<Pie> pies = new LinkedList<Pie>();
        private LinkedList<Spit> spits = new LinkedList<Spit>();

        /// <summary>
        /// pies that have been fired
        /// </summary>
        private LinkedList<Pie> firedPies = new LinkedList<Pie>();
        private LinkedList<Spit> firedSpit = new LinkedList<Spit>();

        /// <summary>
        /// pies that are stuck to the wall
        /// </summary>
        private LinkedList<Pie> stuckPies = new LinkedList<Pie>();

        /// <summary>
        /// the pie model (probably shouldn't be saved here)
        /// </summary>
        private Model pieModel = null;

        /// <summary>
        /// indicates whether a pie was just fired
        /// </summary>
        bool canFire = true;

        //
        // Player location information.  We keep a x/z location (y stays zero)
        // and an orientation (which way we are looking).
        //

        /// <summary>
        /// Player location in the prison. Only x/z are important. y still stay zero
        /// unless we add some flying or jumping behavior later on.
        /// </summary>
        private Vector3 location = new Vector3(275, 0, 1053);

        /// <summary>
        /// The player orientation as a simple angle
        /// </summary>
        private float orientation = 4f;

        private Vector3 alienLocation = new Vector3(0, 0, 500);
        private float alienOrientation = 0;

        /// <summary>
        /// The player transformation matrix. Places the player where they need to be.
        /// </summary>
        private Matrix transform;

        /// <summary>
        /// The rotation rate in radians per second when player is rotating
        /// </summary>
        private float panRate = 2;

        /// <summary>
        /// The player move rate in centimeters per second
        /// </summary>
        private float moveRate = 500;

        /// <summary>
        /// Id for a door we are opening or 0 if none.
        /// </summary>
        private int openDoor = 0;

        /// <summary>
        /// Keeps track of the last game pad state
        /// </summary>
        GamePadState lastGPS;

        /// <summary>
        /// dalek
        /// </summary>
        //private Dalek dalek = null;
        private DalekAnimatedModel dalek = null;

        /// <summary>
        /// alien
        /// </summary>
        private AlienAnimatedModel alien = null;

        /// <summary>
        /// the section of the player
        /// </summary>
        int section = 1;

        /// <summary>
        /// the section on the other side of the door
        /// </summary>
        int otherSection = 2;

        /// <summary>
        /// how open the door is
        /// </summary>
        Vector3 gain = Vector3.Zero;

        private Dictionary<string, List<Vector2>> regions = new Dictionary<string, List<Vector2>>();

        private enum States { Start, StanceStart, Stance, WalkStart, WalkLoopStart, 
            WalkLoop, TurningRightLoopStart, TurningLeftLoopStart, TurnRight, TurnLeft,
            RaiseBazooka, LowerBazooka, Shooting}
        private States state = States.Start;
        private enum AlienStates
        { Start, StanceStart, Stance, WalkStart, WalkLoopStart,
            WalkLoop, TurningRightLoopStart, TurningLeftLoopStart, TurnRight, TurnLeft, Eating, EatingStart}
        private AlienStates alienState = AlienStates.Start;

        public Vector3 Gain { get { return gain; } set { gain = value; } }
        public int Section { get { return section; } }
        public int OtherSection { get { return otherSection; } }
        public Vector3 Location { get { return location; } }
        public float Orientation { get { return orientation; } }
        public Matrix Transform { get { return transform; } }
        public float AlienSpitVelocity { get { return alienSpitVelocity; } }

        #endregion


        public Player(PrisonGame game)
        {
            this.game = game;
            SetPlayerTransform();

            //bazooka = new Bazooka(game);
            for (int i = 0; i < 10; i++)
            {
                Pie pie = new Pie(game, i%3+1);
                pies.AddFirst(pie);
            }
            for (int i = 0; i < 20; i++)
            {
                Spit spit = new Spit(game);
                spits.AddFirst(spit);
            }

            // add other alien still
            dalek = new DalekAnimatedModel(game, "Dalek");
            dalek.Position = Vector3.Zero;
            alien = new AlienAnimatedModel(game, "Alien");
            alien.Position = Vector3.Zero;
            alien.AddAssetClip("stance", "Alien-stance");
            alien.AddAssetClip("walkstart", "Alien-walkstart");
            alien.AddAssetClip("walkloop", "Alien-walkloop");
            alien.AddAssetClip("catcheat", "Alien-catcheat");
            alien.AddAssetClip("tantrum", "Alien-trantrum");

            victoria = new AnimatedModel(game, "Victoria");
            victoria.AddAssetClip("dance", "Victoria-dance");
            victoria.AddAssetClip("stance", "Victoria-stance");
            victoria.AddAssetClip("walk", "Victoria-walk");
            victoria.AddAssetClip("walkstart", "Victoria-walkstart");
            victoria.AddAssetClip("walkloop", "Victoria-walkloop");
            victoria.AddAssetClip("leftturn", "Victoria-leftturn");
            victoria.AddAssetClip("rightturn", "Victoria-rightturn");
            // Bazooka animations
            victoria.AddAssetClip("crouchbazooka", "Victoria-crouchbazooka");
            victoria.AddAssetClip("lowerbazooka", "Victoria-lowerbazooka");
            victoria.AddAssetClip("raisebazooka", "Victoria-raisebazooka");
            victoria.AddAssetClip("walkloopbazooka", "Victoria-walkloopbazooka");
            victoria.AddAssetClip("walkstartbazooka", "Victoria-walkstartbazooka");
        }

        public void Initialize()
        {
            lastGPS = GamePad.GetState(PlayerIndex.One);
        }

        /// <summary>
        /// Set the value of transform to match the current location
        /// and orientation.
        /// </summary>
        private void SetPlayerTransform()
        {
            transform = Matrix.CreateRotationY(orientation);
            transform.Translation = location;
        }
        /// <summary>
        /// set value of alien transform
        /// </summary>
        private void SetAlienTransform()
        {
            transform = Matrix.CreateRotationY(orientation);
            transform.Translation = location;
        }


        public void LoadContent(ContentManager content)
        {
            Model model = content.Load<Model>("AntonPhibesCollision");
            victoria.LoadContent(content);
            dalek.LoadContent(content);
            alien.LoadContent(content);
            foreach (Pie pie in pies)
            {
                 pie.LoadContent(content);
            }
            foreach (Pie pie in firedPies)
            {
                pie.LoadContent(content);
            }
            foreach (Pie pie in stuckPies)
            {
                pie.LoadContent(content);
            }
            foreach (Spit spit in spits)
            {
                spit.LoadContent(content);
            }
            foreach (Spit spit in firedSpit)
            {
                spit.LoadContent(content);
            }

            victoria.SpineBone = victoria.GetBoneIndex("Bip01 Spine1");
            victoria.HandBone = victoria.GetBoneIndex("Bip01 R Hand");

            Matrix[] M = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(M);

            foreach (ModelMesh mesh in model.Meshes)
            {
                // For accumulating the triangles for this mesh
                List<Vector2> triangles = new List<Vector2>();

                // Loop over the mesh parts
                foreach(ModelMeshPart meshPart in mesh.MeshParts)
                {
                    // 
                    // Obtain the vertices for the mesh part
                    //

                    int numVertices = meshPart.VertexBuffer.VertexCount;
                    VertexPositionColorTexture[] verticesRaw = new VertexPositionColorTexture[numVertices];
                    meshPart.VertexBuffer.GetData<VertexPositionColorTexture>(verticesRaw);

                    //
                    // Obtain the indices for the mesh part
                    //

                    int numIndices = meshPart.IndexBuffer.IndexCount;
                    short [] indices = new short[numIndices];
                    meshPart.IndexBuffer.GetData<short>(indices);

                    //
                    // Build the list of triangles
                    //

                    for (int i = 0; i < meshPart.PrimitiveCount * 3; i++)
                    {
                        // The actual index is relative to a supplied start position
                        int index = i + meshPart.StartIndex;

                        // Transform the vertex into world coordinates
                        Vector3 v = Vector3.Transform(verticesRaw[indices[index] + meshPart.VertexOffset].Position, M[mesh.ParentBone.Index]);
                        triangles.Add(new Vector2(v.X, v.Z));
                    }

                }

                regions[mesh.Name] = triangles;
            }

            AnimationPlayer player = victoria.PlayClip("walk");
            AnimationPlayer playerAlien = alien.PlayClip("walkstart");
            pieModel = pies.First.Value.Model;
        }

        private string TestRegion(Vector3 v3)
        {
            // Convert to a 2D Point
            float x = v3.X;
            float y = v3.Z;

            foreach (KeyValuePair<string, List<Vector2>> region in regions)
            {
                // For now we ignore the walls
                if(region.Key.StartsWith("W"))
                    continue;

                for (int i = 0; i < region.Value.Count; i += 3)
                {
                    float x1 = region.Value[i].X;
                    float x2 = region.Value[i + 1].X;
                    float x3 = region.Value[i + 2].X;
                    float y1 = region.Value[i].Y;
                    float y2 = region.Value[i + 1].Y;
                    float y3 = region.Value[i + 2].Y;

                    float d = 1.0f / ((x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3));
                    float l1 = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) * d;
                    if (l1 < 0)
                        continue;

                    float l2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) * d;
                    if (l2 < 0)
                        continue;

                    float l3 = 1 - l1 - l2;
                    if (l3 < 0)
                        continue;

                    return region.Key;
                }
            }

            return "";
        }

        private float GetDesiredSpeed(ref KeyboardState keyboardState, ref GamePadState gamePadState)
        {
            if (keyboardState.IsKeyDown(Keys.Up))
                return 2;

            float speed = gamePadState.ThumbSticks.Right.Y;

            // I'm not allowing you to walk backwards
            if (speed < 0)
                speed = 0;

            return speed;
        }

        private float GetDesiredTurnRate(ref KeyboardState keyboardState, ref GamePadState gamePadState)
        {
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                return panRate;
            }

            if (keyboardState.IsKeyDown(Keys.Right))
            {
                return -panRate;
            }

            return -gamePadState.ThumbSticks.Right.X * panRate;
        }

        private void SetAdjacentRoom(int doorNum)
        {
            switch (doorNum)
            {
                case 1:
                    if (section == 1)
                        otherSection = 2;
                    else
                        otherSection = 1;
                    break;

                case 2:
                    if (section == 2)
                        otherSection = 3;
                    else
                        otherSection = 2;
                    break;

                case 3:
                    if (section == 3)
                        otherSection = 4;
                    else
                        otherSection = 3;
                    break;

                case 4:
                    if (section == 4)
                        otherSection = 3;
                    else
                        otherSection = 4;
                    break;

                case 5:
                    if (section == 5)
                        otherSection = 4;
                    else
                        otherSection = 5;
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            //openDoor = 0;
            KeyboardState keyboardState = Keyboard.GetState();
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            float speed = 0;
            double deltaTotal = gameTime.ElapsedGameTime.TotalSeconds;

            do
            {
                double delta = deltaTotal;

                //
                // State machine will go here
                //

                switch (state)
                {
                    case States.Start:
                        state = States.StanceStart;
                        delta = 0;
                        break;

                    case States.StanceStart: 
                        //victoria.PlayClip("stance");
                        victoria.PlayClip("raisebazooka");
                        victoria.Player.Speed = 0;
                        state = States.Stance;
                        location.Y = 0;
                        break;

                    case States.Stance:
                        speed = GetDesiredSpeed(ref keyboardState, ref gamePadState);

                        if (speed > 0)
                        {
                            // We need to leave the stance state and start walking
                            //victoria.PlayClip("walkstart");
                            victoria.PlayClip("walkstartbazooka");
                            victoria.Player.Speed = speed;
                            state = States.WalkStart;
                        }
                        else
                        {
                            if (keyboardState.IsKeyDown(Keys.Right))
                            {
                                state = States.TurningRightLoopStart;
                            }
                            else if (keyboardState.IsKeyDown(Keys.Left))
                            {
                                state = States.TurningLeftLoopStart;
                            }
                            else if (keyboardState.IsKeyDown(Keys.Space))
                            {
                                victoria.PlayClip("raisebazooka");
                                state = States.RaiseBazooka;
                            }
                        }

                        break;

                    case States.RaiseBazooka:
                        if (delta > victoria.Player.Clip.Duration - victoria.Player.Time)
                        {
                            delta = victoria.Player.Clip.Duration - victoria.Player.Time;

                            // The clip is done after this update
                            state = States.Shooting;
                        }
                        else
                            victoria.Bazooka.Offset = new Vector3(victoria.Bazooka.Offset.X - 0.3f, victoria.Bazooka.Offset.Y + 0.7f, victoria.Bazooka.Offset.Z);
                        break;

                    case States.LowerBazooka:
                        if (delta > victoria.Player.Clip.Duration - victoria.Player.Time)
                        {
                            delta = victoria.Player.Clip.Duration - victoria.Player.Time;

                            // The clip is done after this update
                            state = States.StanceStart;
                        }
                        else
                            victoria.Bazooka.Offset = new Vector3(victoria.Bazooka.Offset.X + 0.3f, victoria.Bazooka.Offset.Y - 0.7f, victoria.Bazooka.Offset.Z);
                        break;

                    case States.Shooting:
                        if (keyboardState.IsKeyDown(Keys.C) || keyboardState.IsKeyDown(Keys.V))
                        {
                            victoria.PlayClip("lowerbazooka");
                            state = States.LowerBazooka;
                        }
                        else if (keyboardState.IsKeyDown(Keys.Space) && canFire == true && pies.Count > 0)
                        {
                            canFire = false;
                            Pie pie = pies.Last.Value;
                            pies.RemoveLast();

                            pie.Velocity = new Vector3(0, 0, -6f);
                            //pie.Gravity = -0.0000003f;
                            firedPies.AddFirst(pie);
                        }
                        else if (keyboardState.IsKeyDown(Keys.Left))
                        {
                            victoria.SpineOrientation = new Vector3(victoria.SpineOrientation.X + 0.03f,
                                victoria.SpineOrientation.Y, victoria.SpineOrientation.Z);
                        }
                        else if (keyboardState.IsKeyDown(Keys.Right))
                        {
                            victoria.SpineOrientation = new Vector3(victoria.SpineOrientation.X - 0.03f,
                                victoria.SpineOrientation.Y, victoria.SpineOrientation.Z);
                        }
                        else if (keyboardState.IsKeyDown(Keys.Up))
                        {
                            victoria.SpineOrientation = new Vector3(victoria.SpineOrientation.X,
                                victoria.SpineOrientation.Y, victoria.SpineOrientation.Z + 0.03f);
                        }
                        else if (keyboardState.IsKeyDown(Keys.Down))
                        {
                            victoria.SpineOrientation = new Vector3(victoria.SpineOrientation.X,
                                victoria.SpineOrientation.Y, victoria.SpineOrientation.Z - 0.03f);
                        }

                        if (keyboardState.IsKeyUp(Keys.Space))
                            canFire = true;

                        break;

                    case States.TurningRightLoopStart:
                        //victoria.PlayClip("rightturn");
                        victoria.PlayClip("raisebazooka");
                        victoria.Player.Speed = 0;
                        state = States.TurnRight;
                        break;
                    case States.TurningLeftLoopStart:
                        //victoria.PlayClip("leftturn");
                        victoria.PlayClip("raisebazooka");
                        victoria.Player.Speed = 0;
                        state = States.TurnLeft;
                        break;

                    case States.TurnRight:
                        if (delta > victoria.Player.Clip.Duration - victoria.Player.Time)
                        {
                            delta = victoria.Player.Clip.Duration - victoria.Player.Time;

                            // The clip is done after this update
                            state = States.TurningRightLoopStart;
                        }

                        if (!keyboardState.IsKeyDown(Keys.Right) || keyboardState.IsKeyDown(Keys.Up))
                        {
                            delta = 0;
                            state = States.StanceStart;
                        }

                        break;
                    case States.TurnLeft:
                        if (delta > victoria.Player.Clip.Duration - victoria.Player.Time)
                        {
                            delta = victoria.Player.Clip.Duration - victoria.Player.Time;

                            // The clip is done after this update
                            state = States.TurningLeftLoopStart;
                        }

                        if (!keyboardState.IsKeyDown(Keys.Left) || keyboardState.IsKeyDown(Keys.Up))
                        {
                            delta = 0;
                            state = States.StanceStart;
                        }

                        break;

                    case States.WalkStart:
                    case States.WalkLoop:
                        if (delta > victoria.Player.Clip.Duration - victoria.Player.Time)
                        {
                            delta = victoria.Player.Clip.Duration - victoria.Player.Time;

                            // The clip is done after this update
                            state = States.WalkLoopStart;
                        }

                        speed = GetDesiredSpeed(ref keyboardState, ref gamePadState);
                        if (speed == 0)
                        {
                            delta = 0;
                            state = States.StanceStart;
                        }
                        else
                        {
                            victoria.Player.Speed = speed;
                        }

                        location.Y = 0;

                        break;

                    case States.WalkLoopStart:
                        //victoria.PlayClip("walkloop").Speed = GetDesiredSpeed(ref keyboardState, ref gamePadState);
                        victoria.PlayClip("walkloopbazooka").Speed = GetDesiredSpeed(ref keyboardState, ref gamePadState);
                        state = States.WalkLoop;
                        break;
                }
                
                // 
                // State update
                //

                if (state != States.Shooting)
                {
                    orientation += GetDesiredTurnRate(ref keyboardState, ref gamePadState) * (float)delta;
                }

                victoria.Update(delta);

                //
                // dalek
                //

                TestDalekCollision();
                dalek.SetDalekHeadRotation(location);
                dalek.Update(delta);

                //
                // Part 1:  Compute a new orientation
                //

                Matrix deltaMatrix = victoria.DeltaMatrix;
                float deltaAngle = (float)Math.Atan2(deltaMatrix.Backward.X, deltaMatrix.Backward.Z);
                float newOrientation = orientation + deltaAngle;

                //
                // Part 2:  Compute a new location
                //

                // We are likely rotated from the angle the model expects to be in
                // Determine that angle.
                Matrix rootMatrix = victoria.RootMatrix;
                float actualAngle = (float)Math.Atan2(rootMatrix.Backward.X, rootMatrix.Backward.Z);
                Vector3 newLocation = location + Vector3.TransformNormal(victoria.DeltaPosition,
                               Matrix.CreateRotationY(newOrientation - actualAngle));

                //
                // I'm just taking these here.  You'll likely want to add something 
                // for collision detection instead.
                //

                bool collision = false;     // Until we know otherwise

                string region = TestRegion(newLocation);

                // Slimed support
                if (region == "R_Section6")
                {
                    section = 6;

                    if (!game.Slimed)
                        game.Slimed = true;
                }
                else if (region == "R_Section5")
                {
                    section = 5;
                }
                else if (region == "R_Section4")
                {
                    section = 4;
                }
                else if (region == "R_Section3")
                {
                    section = 3;
                }
                else if (region == "R_Section2")
                {
                    section = 2;
                }
                else if (region == "R_Section1")
                {
                    section = 1;
                    ReloadPies();

                    if (game.Slimed)
                        game.Slimed = false;
                }

                if (region == "")
                {
                    // If not in a region, we have stepped out of bounds
                    collision = true;
                }
                else if (region.StartsWith("R_Door"))   // Are we in a door region
                {
                    // What is the door number for the region we are in?
                    int dnum = int.Parse(region.Substring(6));

                    // Are we currently facing the door or walking through a 
                    // door?

                    bool underDoor;
                    if (DoorShouldBeOpen(dnum, location, transform.Backward, out underDoor))
                    {
                        SetOpenDoor(dnum);
                        SetAdjacentRoom(dnum);
                    }
                    else
                    {
                        SetOpenDoor(0);
                    }

                    if (underDoor)
                    {
                        // is the door actually open right now?
                        bool isOpen = false;
                        foreach (PrisonModel model in game.PhibesModels)
                        {
                            if (model.DoorIsOpen(dnum))
                            {
                                isOpen = true;
                                break;
                            }
                        }

                        if (!isOpen)
                            collision = true;
                    }
                }
                else if (openDoor > 0)
                {
                    // Indicate none are open
                    SetOpenDoor(0);
                }

                if (!collision)
                {
                    location = newLocation;
                }

                orientation = newOrientation;
                SetPlayerTransform();


                deltaTotal -= delta;
            } while (deltaTotal > 0);

            //
            // alien states
            //

            deltaTotal = gameTime.ElapsedGameTime.TotalSeconds;
            do
            {
                double delta = deltaTotal;

                //
                // state machine
                //

                switch (alienState)
                {
                    case AlienStates.Start:
                        alienState = AlienStates.StanceStart;
                        delta = 0;
                        break;

                    case AlienStates.StanceStart:
                        alien.PlayClip("stance");
                        alienState = AlienStates.Stance;
                        alienLocation.Y = 0;
                        break;

                    case AlienStates.Stance:
                        speed = 2f; // GetDesiredSpeed(ref keyboardState, ref gamePadState);

                        // We need to leave the stance state and start walking
                        alien.PlayClip("walkstart");
                        alien.Player.Speed = speed;
                        alienState = AlienStates.WalkStart;
                        break;

                    case AlienStates.WalkStart:
                    case AlienStates.WalkLoop:
                        if (delta > alien.Player.Clip.Duration - alien.Player.Time)
                        {
                            delta = alien.Player.Clip.Duration - alien.Player.Time;

                            // The clip is done after this update
                            alienState = AlienStates.WalkLoopStart;
                        }

                        //speed = GetDesiredSpeed(ref keyboardState, ref gamePadState);
                        speed = 2f;
                        if (speed == 0)
                        {
                            delta = 0;
                            alienState = AlienStates.StanceStart;
                        }
                        else
                        {
                            alien.Player.Speed = speed;
                        }

                        alienLocation.Y = 0;

                        break;

                    case AlienStates.WalkLoopStart:
                        alien.PlayClip("walkloop").Speed = 2f;
                        alienState = AlienStates.WalkLoop;
                        break;

                    case AlienStates.EatingStart:
                        alien.PlayClip("catcheat").Speed = 1f;
                        alien.Player.Speed = speed;
                        alienState = AlienStates.Eating;
                        break;

                    case AlienStates.Eating:
                        if (delta > alien.Player.Clip.Duration - alien.Player.Time)
                        {
                            delta = alien.Player.Clip.Duration - alien.Player.Time;

                            // The clip is done after this update
                            alienState = AlienStates.WalkLoopStart;
                        }

                        speed = 1f;
                        if (speed == 0)
                        {
                            delta = 0;
                            alienState = AlienStates.StanceStart;
                        }
                        else
                        {
                            alien.Player.Speed = speed;
                        }

                        break;
                }

                //
                // alien collision
                //

                TestAlienCollision();

                //
                // alien
                //

                alien.Update(delta);

            deltaTotal -= delta;
            } while (deltaTotal > 0);

            //
            // collision detection for the pies
            //

            LinkedList<Pie> tempPies = new LinkedList<Pie>(); // keep track of pies that hit a wall
            foreach (Pie pie in firedPies)
            {
                pie.Velocity = pie.Velocity + new Vector3(pie.Velocity.X, pie.Velocity.Y, 5f);
                string region = TestRegion(pie.Transform.Translation);
                if (region == "")
                {
                    tempPies.AddFirst(pie); // collision with wall
                }
                else if (region.StartsWith("R_Door"))
                {
                    // What is the door number for the region we are in?
                    int dnum = int.Parse(region.Substring(6));

                    bool isOpen = false;
                    foreach (PrisonModel model in game.PhibesModels)
                    {
                        if (model.DoorIsOpen(dnum))
                        {
                            isOpen = true;
                            break;
                        }
                    }

                    if (!isOpen)
                        tempPies.AddFirst(pie); // collision with door
                }
            }
            // remove all the pies that are //stuck
            foreach (Pie pie in tempPies)
            {
                pie.Spin = 2.2f;
                stuckPies.AddFirst(pie);
                firedPies.Remove(pie);
            }

            //
            // collision detection for spit
            //
            LinkedList<Spit> tempSpit = new LinkedList<Spit>(); // keep track of pies that hit a wall
            //foreach (Spit spit in firedSpit)
            //{
            //    spit.Velocity = spit.Velocity + new Vector3(spit.Velocity.X, spit.Velocity.Y, 5f);
            //    string region = TestRegion(spit.Transform.Translation);
            //    if (region == "")
            //    {
            //        tempSpit.AddFirst(spit); // collision with wall
            //    }
            //    else if (region.StartsWith("R_Door"))
            //    {
            //        // What is the door number for the region we are in?
            //        int dnum = int.Parse(region.Substring(6));

            //        bool isOpen = false;
            //        foreach (PrisonModel model in game.PhibesModels)
            //        {
            //            if (model.DoorIsOpen(dnum))
            //            {
            //                isOpen = true;
            //                break;
            //            }
            //        }

            //        if (!isOpen)
            //            tempSpit.AddFirst(spit); // collision with door
            //    }
            //}
            //foreach (Spit spit in tempSpit)
            //{
            //    spit.Spin = 2.2f;
            //    spits.AddFirst(spit);
            //    firedSpit.Remove(spit);
            //}

            //
            // camera follows player
            //

            Vector3 newEye = Vector3.Transform(new Vector3(0, 190, -250), transform);
            string camRegion = TestRegion(newEye);
            if (camRegion != "")
            {
                // implement chase camera
                game.Camera.Eye = newEye;
                game.Camera.Center = location + new Vector3(0, 120, 0);
            }

            //bazooka.Position = location;
            //bazooka.Orientation = orientation;

            //bazooka.Update(gameTime);

            foreach (Pie pie in pies)
            {
                pie.Orientation = orientation;
                pie.Position = victoria.Bazooka.Transform.Translation;
                pie.Update(gameTime);
            }
            LinkedList<Pie> tempPiesToDelete = new LinkedList<Pie>(); // keep track of pies that hit a wall
            foreach (Pie pie in firedPies)
            {
                // Obtain a bounding sphere for the pie.  I can get away
                // with this here because I know the model has exactly one mesh
                // and exactly one bone.
                BoundingSphere bs = pieModel.Meshes[0].BoundingSphere;
                bs = bs.Transform(pieModel.Bones[0].Transform);

                // Move this to world coordinates.  Note how easy it is to 
                // transform a bounding sphere
                bs.Radius *= 1f; /*pie.size;*/
                //bs.Center += pie.Position + pie.Offset - pie.OrigOffset;
                bs.Center = pie.Transform.Translation;

                // test if pie hit dalek
                if (dalek.TestSphereForCollision(bs, Vector3.Zero))
                {
                    tempPiesToDelete.AddFirst(pie);
                    // TODO: trace statement that prints out the location of the collision between dalek and the pie
                    continue;
                }
                // test if pie hit alien
                else if (alien.TestSphereForCollision(bs, alienLocation))
                {
                    alienState = AlienStates.EatingStart;
                    tempPiesToDelete.AddFirst(pie);
                    continue;
                }

                pie.Spin += 0.1f;
                pie.Update(gameTime);
            }
            // remove all the pies that are //stuck
            foreach (Pie pie in tempPiesToDelete)
            {
                pie.Spin = 2.2f;
                firedPies.Remove(pie);
            }

            // collision detection for spit
            LinkedList<Spit> tempSpitToDelete = new LinkedList<Spit>(); // keep track of pies that hit a wall
            foreach (Spit spit in firedSpit)
            {
                // Obtain a bounding sphere for the pie.  I can get away
                // with this here because I know the model has exactly one mesh
                // and exactly one bone.
                BoundingSphere bs = pieModel.Meshes[0].BoundingSphere;
                bs = bs.Transform(pieModel.Bones[0].Transform);

                // Move this to world coordinates.  Note how easy it is to 
                // transform a bounding sphere
                bs.Radius *= 1f; /*pie.size;*/
                //bs.Center += pie.Position + pie.Offset - pie.OrigOffset;
                bs.Center = spit.Transform.Translation;

                // test if pie hit dalek
                if (victoria.TestSphereForCollision(bs, location))
                {
                    tempSpitToDelete.AddFirst(spit);
                    game.Slimed = true;
                    continue;
                }

                spit.Spin += 0.1f;
                spit.Update(gameTime);
            }
            // remove all the pies that are //stuck
            foreach (Spit spit in tempSpitToDelete)
            {
                spits.AddFirst(spit);
                firedSpit.Remove(spit);
            }

            //
            // spit when needed
            //
            SpitTime += (float)gameTime.ElapsedGameTime.Milliseconds / 1000f;
            System.Diagnostics.Trace.WriteLine("spit time: " + SpitTime);
            if (SpitTime >= SpitFrequency)
            {
                // spit
                Spit spit = spits.First.Value;
                firedSpit.AddFirst(spit);
                spit.Position = new Vector3(alienLocation.X, 150f, alienLocation.Z);
                spit.Velocity = new Vector3(0, 0, alien.Velocity * 2f);
                spits.Remove(spit);
                SpitTime = 0;
            }

            /*
            // test the alien collision
            foreach (ModelMesh mesh in dalek.Model.Meshes)
            {
                BoundingSphere bs = mesh.BoundingSphere;
                bs = bs.Transform(transforms[mesh.ParentBone.Index] * dalek.Transform);
                if (TestSphereForCollision(bs))
                {
                    // eat pie
                }
            }
            */
            
        }

        private void TestAlienCollision()
        {
            Vector3 newLocation = new Vector3(alienLocation.X, alienLocation.Y,
                alienLocation.Z + alien.Velocity);
            bool collision = false;     // Until we know otherwise

            string region = TestRegion(newLocation);

            if (region == "")
            {
                // If not in a region, we have stepped out of bounds
                collision = true;
            }
            else if (region.StartsWith("R_Door"))   // Are we in a door region
            {
                // What is the door number for the region we are in?
                int dnum = int.Parse(region.Substring(6));

                // is the door actually open right now?
                bool isOpen = false;
                foreach (PrisonModel model in game.PhibesModels)
                {
                    if (model.DoorIsOpen(dnum))
                    {
                        isOpen = true;
                        break;
                    }
                }

                if (!isOpen)
                    collision = true;
            }

            if (collision)
            {
                alien.Velocity = -alien.Velocity;
                alienSpitVelocity = -alienSpitVelocity;
                alienOrientation = alienOrientation == 3f ? 0 : 3f;
            }

            alienLocation = new Vector3(alienLocation.X, alienLocation.Y,
                alienLocation.Z + alien.Velocity);
        }
        
        private void TestDalekCollision()
        {
            Vector3 newLocation = new Vector3(dalek.Position.X, dalek.Position.Y,
                dalek.Position.Z + dalek.Velocity);
            bool collision = false;     // Until we know otherwise

            string region = TestRegion(newLocation);

            if (region == "")
            {
                // If not in a region, we have stepped out of bounds
                collision = true;
            }
            else if (region.StartsWith("R_Door"))   // Are we in a door region
            {
                // What is the door number for the region we are in?
                int dnum = int.Parse(region.Substring(6));

                // is the door actually open right now?
                bool isOpen = false;
                foreach (PrisonModel model in game.PhibesModels)
                {
                    if (model.DoorIsOpen(dnum))
                    {
                        isOpen = true;
                        break;
                    }
                }

                if (!isOpen)
                    collision = true;
            }

            if (collision)
            {
                dalek.Velocity = -dalek.Velocity;
                dalek.Orientation = dalek.Orientation == 3f ? 0 : 3f;
            }
        }
        

        public void ReloadPies()
        {
            int i = pies.Count;
            while (i < 10)
            {
                Pie pie = new Pie(game, i % 3 + 1);
                pie.Model = pieModel;
                pies.AddFirst(pie);
                i++;
            }
        }

        public void Draw(GraphicsDeviceManager graphics, GameTime gameTime)
        {
            Matrix transform = Matrix.CreateRotationY(orientation);
            transform.Translation = location;

            victoria.Draw(graphics, gameTime, transform);
            dalek.Draw(graphics, gameTime, Matrix.Identity);

            transform = Matrix.CreateRotationY(alienOrientation);
            transform.Translation = alienLocation;
            alien.Draw(graphics, gameTime, transform);

            foreach (Pie pie in pies)
            {
                pie.Draw(graphics, gameTime);
            }
            foreach (Pie pie in firedPies)
            {
                pie.Draw(graphics, gameTime);
            }
            foreach (Pie pie in stuckPies)
            {
                pie.Draw(graphics, gameTime);
            }
            foreach (Spit spit in firedSpit)
            {
                spit.Draw(graphics, gameTime);
            }
        }

        /// <summary>
        /// This is the logic that determines if a door should be open.  This is 
        /// based on a position and a direction we are traveling.  
        /// </summary>
        /// <param name="dnum">Door number we are interested in (1-5)</param>
        /// <param name="loc">A location near the door</param>
        /// <param name="dir">Direction we are currently facing as a vector.</param>
        /// <param name="doorVector">A vector pointing throught the door.</param>
        /// <param name="doorCenter">The center of the door.</param>
        /// <param name="under">Return value - indicates we are under the door</param>
        /// <returns>True if we are under the door</returns>
        private bool DoorShouldBeOpen(int dnum, Vector3 loc, Vector3 dir, out bool under)
        {
            Vector3 doorCenter;
            Vector3 doorVector;

            // I need to know information about the doors.  This 
            // is the location and a vector through the door for each door.
            switch (dnum)
            {
                case 1:
                    doorCenter = new Vector3(218, 0, 1023);
                    doorVector = new Vector3(1, 0, 0);
                    break;

                case 2:
                    doorCenter = new Vector3(-11, 0, -769);
                    doorVector = new Vector3(0, 0, 1);
                    break;

                case 3:
                    doorCenter = new Vector3(587, 0, -999);
                    doorVector = new Vector3(1, 0, 0);
                    break;

                case 4:
                    doorCenter = new Vector3(787, 0, -763);
                    doorVector = new Vector3(0, 0, 1);
                    break;

                case 5:
                default:
                    doorCenter = new Vector3(1187, 0, -1218);
                    doorVector = new Vector3(0, 0, 1);
                    break;
            }

            // I want the door vector to indicate the direction we are doing through the
            // door.  This depends on the side of the center we are on.
            Vector3 toDoor = doorCenter - loc;
            if (Vector3.Dot(toDoor, doorVector) < 0)
            {
                doorVector = -doorVector;
            }


            // Determine if we are under the door
            // Determine points after the center where we are 
            // considered to be under the door
            Vector3 doorBefore = doorCenter - doorVector * DoorUnderRange;
            Vector3 doorAfter = doorCenter + doorVector * DoorUnderRange;
            under = false;

            // If we have passed the point before the door, a vector 
            // to our position from that point will be pointing within 
            // 90 degrees of the door vector.  
            if (Vector3.Dot(loc - doorAfter, doorVector) <= 0 &&
                Vector3.Dot(loc - doorBefore, doorVector) >= 0)
            {
                under = true;
                return true;
            }

            // Are we facing the door?
            if (Vector3.Dot(dir, doorVector) >= 0)
            {
                // We are, so the door should be open
                return true;
            }

            return false;
        }


        /// <summary>
        /// Set the current open/opening door
        /// </summary>
        /// <param name="dnum">Door to set open or 0 if none</param>
        private void SetOpenDoor(int dnum)
        {
            // Is this already indicated?
            if (openDoor == dnum)
                return;

            // Is a door other than this already open?
            // If so, make it close
            if (openDoor > 0 && openDoor != dnum)
            {
                foreach (PrisonModel model in game.PhibesModels)
                {
                    model.SetDoor(openDoor, false);
                }
            }

            // Make this the open door and flag it as open
            openDoor = dnum;
            if (openDoor > 0)
            {
                foreach (PrisonModel model in game.PhibesModels)
                {
                    model.SetDoor(openDoor, true);
                }
            }
        }

        /// <summary>
        /// Tests a point to see if it is in the bounding sphere of any of 
        /// our pies.
        /// </summary>
        /// <param name="position">Tip of the laser</param>
        /// <returns></returns>
        public bool TestSphereForCollision(BoundingSphere sphere, Vector3 location)
        {
            for (LinkedListNode<Pie> pieNode = pies.First; pieNode != null; )
            {
                LinkedListNode<Pie> nextNode = pieNode.Next;
                Pie pie = pieNode.Value;

                // Obtain a bounding sphere for the pie.  I can get away
                // with this here because I know the model has exactly one mesh
                // and exactly one bone.
                BoundingSphere bs = pieModel.Meshes[0].BoundingSphere;
                bs = bs.Transform(pieModel.Bones[0].Transform);

                // Move this to world coordinates.  Note how easy it is to 
                // transform a bounding sphere
                bs.Radius *= 1f; /*pie.size;*/
                bs.Center += pie.Position + pie.Offset + pie.OrigOffset;

                if (sphere.Intersects(bs))
                {
                    return true;
                }

                pieNode = nextNode;
            }

            return false;
        }
    }
}
