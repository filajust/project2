﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace PrisonStep
{
    class Dalek
    {
        #region Fields

        /// <summary>
        /// reference to the game
        /// </summary>
        private PrisonGame game;

        /// <summary>
        /// the orientation
        /// </summary>
        float orientation = 0;

        /// <summary>
        /// current position
        /// </summary>
        private Vector3 position = Vector3.Zero;

        /// <summary>
        /// transform
        /// </summary>
        private Matrix transform = Matrix.Identity;

        /// <summary>
        /// the model
        /// </summary>
        private Model model;

        /// <summary>
        /// the asset of dalek
        /// </summary>
        private string asset;

        /// <summary>
        /// the velocity
        /// </summary>
        private float velocity = 3f;

        /// <summary>
        /// the head of dalek
        /// </summary>
        private int head;

        /// <summary>
        /// the plunger arm
        /// </summary>
        private int plungerArm;

        /// <summary>
        /// the second arm
        /// </summary>
        private int arm2;

        /// <summary>
        /// the angle of the head
        /// </summary>
        private float headAngle = 0f;

        /// <summary>
        /// the offset of the headangle
        /// </summary>
        private const float headAngleOffset = -0.4f;

        private double[] lightData =
{   1,      568,      246,    1036,   0.53,   0.53,   0.53,     821,     224, 
  941,  14.2941,       45, 43.9412,    814,    224,   1275,    82.5,       0,  0,
    2,       -5,      169,     428, 0.3964,  0.503, 0.4044,    -5.4,     169,
 1020, 129.4902, 107.5686, 41.8039,   -5.4,    169,   -138, 37.8275,      91, 91,
    3,      113,      217,    -933,    0.5,      0,      0,    -129,     185,
-1085,	     50,        0,       0,    501,    185,  -1087,      48,       0,  0,
    4,      781,      209,    -998,    0.2, 0.1678, 0.1341,    1183,     209,
 -998,	     50,  41.9608, 33.5294,    984,    113,   -932,       0,      80,  0,
    5,      782,      177,    -463,   0.65, 0.5455, 0.4359,     563,     195,
 -197,	     50,        0,       0,   1018,    181,   -188,      80,       0,  0,
    6,     1182,      177,   -1577,   0.65, 0.5455, 0.4359,     971,     181,
-1801,        0,  13.1765,      80,   1406,    181,  -1801,       0, 13.1765,  80};

        #endregion

        #region Properties
        /// <summary>
        /// the position
        /// </summary>
        public Vector3 Position { get { return position; } set { position = value; } }

        /// <summary>
        /// the orientation
        /// </summary>
        public float Orientation { get { return orientation; } set { orientation = value; } }

        /// <summary>
        /// the model of the pies
        /// </summary>
        public Model Model { get { return model; } set { model = value; } }

        /// <summary>
        /// dalek's velocity
        /// </summary>
        public float Velocity { get { return velocity; } set { velocity = value; } }

        #endregion

        #region Construction and Initialization

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="game"></param>
        public Dalek(PrisonGame game, string asset)
        {
            this.game = game;
            this.asset = asset;
        }

        /// <summary>
        /// load the content
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            model = content.Load<Model>(asset);

            // set the index of the head
            head = model.Bones.IndexOf(model.Bones["Head"]);
            plungerArm = model.Bones.IndexOf(model.Bones["PlungerArm"]);
            arm2 = model.Bones.IndexOf(model.Bones["Arm2"]);
        }

        #endregion

        /// <summary>
        /// Set the value of transform to match the current location
        /// and orientation.
        /// </summary>
        private void SetDalekTransform()
        {
            transform = Matrix.CreateRotationY(orientation);
            transform.Translation += position;
        }

        public void SetHeadRotation(Vector3 playerLocation)
        {
            Vector3 direction = playerLocation - position;
            headAngle = (float)Math.Atan2((double)direction.X, (double)direction.Z);
        }

        public void Update(GameTime gameTime)
        {
            position = new Vector3(position.X, position.Y, position.Z + velocity);

            // set the rotation of the destructor
            headAngle += 0.5f;

            // victoria x,z position - dalek position --> direction to victoria
            //    atan2(z/x)
            

            SetDalekTransform();
        }

        public void Draw(GraphicsDeviceManager graphics, GameTime gameTime)
        {
            DrawModel(graphics, model, transform);
        }

        private void DrawModel(GraphicsDeviceManager graphics, Model model, Matrix world)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            int count = model.Bones.Count;
            for (int i = 0; i < count; i++)
            {
                if (i == head || i == plungerArm || i == arm2)
                    transforms[i] = Matrix.CreateRotationZ(headAngle + headAngleOffset);
                else
                    transforms[i] = Matrix.Identity;
                
                ModelBone bone = model.Bones[i];
                if (bone.Parent == null)
                    transforms[i] *= bone.Transform;
                else
                    transforms[i] *= bone.Transform * transforms[bone.Parent.Index];
            }


            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = game.Camera.View;
                    effect.Projection = game.Camera.Projection;

                    /*
                    effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * world);
                    EffectParameter param1 = effect.Parameters["View"];
                    param1.SetValue(game.Camera.View);
                    effect.Parameters["Projection"].SetValue(game.Camera.Projection);

                    EffectParameter param = effect.Parameters["Light1Location"];
                    param.SetValue(LightInfo(game.Player.Section, 0));
                    effect.Parameters["Light1Color"].SetValue(LightInfo(game.Player.Section, 1));
                    effect.Parameters["Light2Location"].SetValue(LightInfo(game.Player.Section, 2));
                    effect.Parameters["Light2Color"].SetValue(LightInfo(game.Player.Section, 3));
                    effect.Parameters["Light3Location"].SetValue(LightInfo(game.Player.Section, 4));
                    effect.Parameters["Light3Color"].SetValue(LightInfo(game.Player.Section, 5));
                    effect.Parameters["Light4Location"].SetValue(LightInfo(game.Player.OtherSection, 0));
                    effect.Parameters["Light4Color"].SetValue(LightInfo(game.Player.OtherSection, 1));
                    effect.Parameters["Gain"].SetValue(game.Player.Gain);
                    */
                }
                mesh.Draw();
            }
        }

        /// <summary>
        /// for getting the correct information about lights
        /// </summary>
        /// <param name="section"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private Vector3 LightInfo(int section, int item)
        {
            int offset = (section - 1) * 19 + 1 + (item * 3);
            return new Vector3((float)lightData[offset],
                               (float)lightData[offset + 1],
                               (float)lightData[offset + 2]);
        }
       
    }
}
