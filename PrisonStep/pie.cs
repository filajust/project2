﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PrisonStep
{
    class Pie
    {
        #region Fields

        /// <summary>
        /// reference to the game
        /// </summary>
        private PrisonGame game;

        /// <summary>
        /// the bazooka orientation
        /// </summary>
        private float orientation = 0;

        /// <summary>
        /// the spin of the pie
        /// </summary>
        private float spin = -1.2f;

        /// <summary>
        /// gravity
        /// </summary>
        private float gravity = 0.0f;

        /// <summary>
        /// which pie will be drawn
        /// </summary>
        private int pieNum = 0;

        /// <summary>
        /// current position
        /// </summary>
        private Vector3 position = Vector3.Zero;

        /// <summary>
        /// offset of pie position
        /// </summary>
        private Vector3 offset = new Vector3(10, 110, 10);

        /// <summary>
        /// offset of pie
        /// </summary>
        private Vector3 origOffset = Vector3.Zero;

        /// <summary>
        /// transform of the pie
        /// </summary>
        private Matrix transform = Matrix.Identity;

        /// <summary>
        /// the model
        /// </summary>
        private Model model;

        /// <summary>
        /// the pie velocity
        /// </summary>
        private Vector3 velocity;

        #endregion

        #region Properties
        /// <summary>
        /// the position
        /// </summary>
        public Vector3 Position { get { return position; } set { position = value; } }

        /// <summary>
        /// the offset
        /// </summary>
        public Vector3 Offset { get { return offset; } set { offset = value; } }

        public Vector3 OrigOffset { get { return origOffset; } set { origOffset = value; } }

        /// <summary>
        /// the velocity
        /// </summary>
        public Vector3 Velocity { get { return velocity; } set { velocity = value; } }

        /// <summary>
        /// the orientation
        /// </summary>
        public float Orientation { get { return orientation; } set { orientation = value; } }

        /// <summary>
        /// the spin of the pie
        /// </summary>
        public float Spin { get { return spin; } set { spin = value; } }

        /// <summary>
        /// the model of the pies
        /// </summary>
        public Model Model { get { return model; } set { model = value; } }

        /// <summary>
        /// return the transform of the pie
        /// </summary>
        public Matrix Transform { get { return transform; } set { transform = value; } }

        /// <summary>
        /// return gravity
        /// </summary>
        public float Gravity { get { return gravity; } set { gravity = value; } }

        #endregion

        #region Construction and Initialization

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="game"></param>
        public Pie(PrisonGame game, int pieNum)
        {
            this.game = game;
            this.pieNum = pieNum;

            switch (pieNum)
            {
                case 1:
                    origOffset = Vector3.Zero;
                    break;

                case 2:
                    origOffset = new Vector3(0, -10, 0);
                    break;

                case 3:
                    origOffset = new Vector3(0, -20, 0);
                    break;

                default:
                    pieNum = 1;
                    origOffset = Vector3.Zero;
                    break;
            }
        }

        /// <summary>
        /// load the content
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            model = content.Load<Model>("pies");
        }

        #endregion

        /// <summary>
        /// Set the value of transform to match the current location
        /// and orientation.
        /// </summary>
        private void SetPieTransform()
        {
            //transform = Matrix.CreateTranslation(origOffset);
            //transform = transform * Matrix.CreateRotationX(spin);
            //transform.Translation += offset;
            //transform = transform * Matrix.CreateRotationY(orientation);
            //transform.Translation += position;
            transform =
                Matrix.CreateTranslation(origOffset) *
                Matrix.CreateRotationX(spin) *
                Matrix.CreateTranslation(velocity) *
                Matrix.CreateRotationY(orientation) *
                Matrix.CreateTranslation(position);
        }

        public void Update(GameTime gameTime)
        {
            velocity = new Vector3(velocity.X, velocity.Y + gravity, velocity.Z);
            SetPieTransform();
        }

        public void Draw(GraphicsDeviceManager graphics, GameTime gameTime)
        {
            DrawModel(graphics, model, transform);
        }

        private void DrawModel(GraphicsDeviceManager graphics, Model model, Matrix world)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            int count = model.Bones.Count;
            for (int i = 0; i < count; i++)
            {
                transforms[i] = Matrix.Identity;
                ModelBone bone = model.Bones[i];
                if (bone.Parent == null)
                    transforms[i] *= bone.Transform;
                else
                    transforms[i] *= bone.Transform * transforms[bone.Parent.Index];
            }


            foreach (ModelMesh mesh in model.Meshes)
            {
                if (pieNum == 1 && mesh.Name.StartsWith("Pie1") ||
                    pieNum == 2 && mesh.Name.StartsWith("Pie2") ||
                    pieNum == 3 && mesh.Name.StartsWith("Pie3"))
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.World = transforms[mesh.ParentBone.Index] * world;
                        // set the camera
                        effect.View = game.Camera.View;
                        effect.Projection = game.Camera.Projection;
                    }
                    mesh.Draw();
                }
            }
        }
    }
}
