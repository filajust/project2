﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrisonStep
{
    public class BoundingCylinder
    {
        /// <summary>
        /// the height of the bounding cylinder
        /// </summary>
        private float height;

        /// <summary>
        /// teh radius
        /// </summary>
        private float radius;

        /// <summary>
        /// point at bottom center of bounding cylinder
        /// </summary>
        private Vector3 location;

        public Vector3 Location { get { return location; } set { location = value; } }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="height"></param>
        /// <param name="radius"></param>
        /// <param name="location"></param>
        public BoundingCylinder(float height, float radius, Vector3 location)
        {
            this.height = height;
            this.radius = radius;
            this.location = location;
        }

        /// <summary>
        /// check intersection with sphere
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public bool Intersect(BoundingSphere bs)
        {
            float cylMin = location.Y;
            float cylMax = location.Y + height;

            // if center of sphere is above cylinder
            if (bs.Center.Y > cylMax)
            {
                Vector3 dir = new Vector3(bs.Center.X - location.X, 0, bs.Center.Z - location.Z);
                dir.Normalize();

                // closest point we can have on cylinder to the sphere
                Vector3 closestPt = new Vector3(location.X + dir.X * radius, cylMax, location.Z + dir.Z * radius);
                if (Vector3.Distance(bs.Center, closestPt) <= radius)
                    return true;
            }
            else
            {
                if (Vector3.Distance(new Vector3(bs.Center.X, 0, bs.Center.Z), new Vector3(location.X, 0, location.Z)) <= radius + bs.Radius)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// check intersection with ray
        /// </summary>
        /// <param name="ray"></param>
        /// <returns></returns>
        public bool Intersect(Ray ray)
        {
            float diffX = ray.Position.X - location.X;
            float diffZ = ray.Position.Z - location.Z;

            float a = ray.Direction.X * ray.Direction.X + ray.Direction.Z * ray.Direction.Z;
            float b = 2 * (ray.Direction.X * diffX + ray.Direction.Z * diffZ);
            float c = diffX * diffX + diffZ * diffZ - (radius * radius);

            // quadratic formula
            float t1 = (-b + (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
            float t2 = (-b - (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

            // t value is closest to us --> ternary operator
            float t = (t1 < t2) ? t1 : t2;

            if (t >= 0)
                return true;
            return false;
        }
    }
}
