﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace PrisonStep
{
    public class Spit
    {
        #region Fields

        /// <summary>
        /// reference to the game
        /// </summary>
        private PrisonGame game;

        /// <summary>
        /// the bazooka orientation
        /// </summary>
        private float orientation = 0;

        /// <summary>
        /// the spin of the pie
        /// </summary>
        private float spin = -1.2f;

        /// <summary>
        /// gravity
        /// </summary>
        private float gravity = 0.0f;

        /// <summary>
        /// current position
        /// </summary>
        private Vector3 position = Vector3.Zero;

        /// <summary>
        /// offset of pie position
        /// </summary>
        private Vector3 offset = new Vector3(10, 110, 10);

        /// <summary>
        /// offset of pie
        /// </summary>
        private Vector3 origOffset = Vector3.Zero;

        /// <summary>
        /// transform of the pie
        /// </summary>
        private Matrix transform = Matrix.Identity;

        /// <summary>
        /// the model
        /// </summary>
        private Model model;

        /// <summary>
        /// the pie velocity
        /// </summary>
        private Vector3 velocity;

        #endregion

        #region Properties
        /// <summary>
        /// the position
        /// </summary>
        public Vector3 Position { get { return position; } set { position = value; } }

        /// <summary>
        /// the offset
        /// </summary>
        public Vector3 Offset { get { return offset; } set { offset = value; } }

        public Vector3 OrigOffset { get { return origOffset; } set { origOffset = value; } }

        /// <summary>
        /// the velocity
        /// </summary>
        public Vector3 Velocity { get { return velocity; } set { velocity = value; } }

        /// <summary>
        /// the orientation
        /// </summary>
        public float Orientation { get { return orientation; } set { orientation = value; } }

        /// <summary>
        /// the spin of the pie
        /// </summary>
        public float Spin { get { return spin; } set { spin = value; } }

        /// <summary>
        /// the model of the pies
        /// </summary>
        public Model Model { get { return model; } set { model = value; } }

        /// <summary>
        /// return the transform of the pie
        /// </summary>
        public Matrix Transform { get { return transform; } set { transform = value; } }

        /// <summary>
        /// return gravity
        /// </summary>
        public float Gravity { get { return gravity; } set { gravity = value; } }

        #endregion

        #region Construction and Initialization

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="game"></param>
        public Spit(PrisonGame game)
        {
            this.game = game;
        }

        /// <summary>
        /// load the content
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            model = content.Load<Model>("spit");
        }

        #endregion

        /// <summary>
        /// Set the value of transform to match the current location
        /// and orientation.
        /// </summary>
        private void SetSpitTransform()
        {
            transform =
                Matrix.CreateRotationX(spin) *
                Matrix.CreateTranslation(velocity) *
                Matrix.CreateRotationY(orientation) *
                Matrix.CreateTranslation(position);
        }

        public void Update(GameTime gameTime)
        {
            velocity = new Vector3(velocity.X, velocity.Y + gravity, velocity.Z + game.Player.AlienSpitVelocity);
            SetSpitTransform();
        }

        public void Draw(GraphicsDeviceManager graphics, GameTime gameTime)
        {
            DrawModel(graphics, model, transform);
        }

        private void DrawModel(GraphicsDeviceManager graphics, Model model, Matrix world)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            int count = model.Bones.Count;
            for (int i = 0; i < count; i++)
            {
                transforms[i] = Matrix.Identity;
                ModelBone bone = model.Bones[i];
                if (bone.Parent == null)
                    transforms[i] *= bone.Transform;
                else
                    transforms[i] *= bone.Transform * transforms[bone.Parent.Index];
            }


            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // set the camera
                    effect.View = game.Camera.View;
                    effect.Projection = game.Camera.Projection;
                }
                mesh.Draw();
            }
        }
    }
}
