﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PrisonStep
{
    public class Bazooka
    {
        #region Fields

        /// <summary>
        /// reference to the game
        /// </summary>
        private PrisonGame game;

        /// <summary>
        /// the bazooka orientation
        /// </summary>
        float orientation = 0;

        /// <summary>
        /// current position
        /// </summary>
        private Vector3 position = Vector3.Zero;

        /// <summary>
        /// offset of bazooka position
        /// </summary>
        private Vector3 offset = new Vector3(10, 110, 10);

        /// <summary>
        /// transform of the bazooka
        /// </summary>
        private Matrix transform = Matrix.Identity;

        /// <summary>
        /// the model
        /// </summary>
        private Model model;

        #endregion

        #region Properties
        /// <summary>
        /// the position
        /// </summary>
        public Vector3 Position { get { return position; } set { position = value; } }

        /// <summary>
        /// the offset
        /// </summary>
        public Vector3 Offset { get { return offset; } set { offset = value; } }

        /// <summary>
        /// the orientation
        /// </summary>
        public float Orientation { get { return orientation; } set { orientation = value; } }

        public Matrix Transform { get { return transform; } set { transform = value; } }

        #endregion

        #region Construction and Initialization
        
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="game"></param>
        public Bazooka(PrisonGame game)
        {
            this.game = game;
        }

        /// <summary>
        /// load the content
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            model = content.Load<Model>("PieBazooka");
        }

        #endregion

        /// <summary>
        /// Set the value of transform to match the current location
        /// and orientation.
        /// </summary>
        private void SetBazookaTransform()
        {
            transform = Matrix.CreateTranslation(offset);
            transform = transform * Matrix.CreateRotationY(orientation);
            transform.Translation += position;
        }

        public void Update(GameTime gameTime)
        {
            //SetBazookaTransform();
        }

        public void Draw(GraphicsDeviceManager graphics, GameTime gameTime)
        {
            DrawModel(graphics, model, transform);
        }

        private void DrawModel(GraphicsDeviceManager graphics, Model model, Matrix world)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            int count = model.Bones.Count;
            for (int i = 0; i < count; i++)
            {
                transforms[i] = Matrix.Identity;
                ModelBone bone = model.Bones[i];
                if (bone.Parent == null)
                    transforms[i] *= bone.Transform;
                else
                    transforms[i] *= bone.Transform * transforms[bone.Parent.Index];
            }

            
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // set the camera
                    effect.View = game.Camera.View;
                    effect.Projection = game.Camera.Projection;

                    //if (game.BatGameScreen.butterflyPower)
                    //{
                    //        PowerLighting(effect);
                   
                    //}
                }
                mesh.Draw();
            }
        }
    }
}
