﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using XnaAux;

namespace PrisonStep
{
    public class AnimatedModel
    {
        /// <summary>
        /// The number of skinning matrices in SkinnedEffect.fx. This must
        /// match the number in SkinnedEffect.fx.
        /// </summary>
        public const int NumSkinBones = 57;

        /// <summary>
        /// Reference to the game that uses this class
        /// </summary>
        protected PrisonGame game;

        /// <summary>
        /// The XNA model we will be animating
        /// </summary>
        protected Model model;

        protected Matrix[] bindTransforms;
        protected Matrix[] boneTransforms;
        protected Matrix[] absoTransforms;

        /// <summary>
        /// Name of the asset we are going to load
        /// </summary>
        private string asset;

        protected Matrix[] skinTransforms = null;

        protected List<int> skelToBone = null;
        protected Matrix[] inverseBindTransforms = null;

        protected AnimationPlayer player = null;

        protected Matrix rootMatrixRaw = Matrix.Identity;
        protected Matrix deltaMatrix = Matrix.Identity;

        public Vector3 DeltaPosition;

        /// <summary>
        /// the orientation of the spine
        /// </summary>
        private Vector3 spineOrientation = Vector3.Zero;
        private Vector3 maxSpineOrientation = new Vector3(0.5f, 0.5f, 0.5f);
        private Vector3 minSpineOrientation = new Vector3(-0.5f, -0.5f, -0.5f);

        /// <summary>
        /// bone that represents different bones
        /// </summary>
        private int spineBone;
        private int handBone;

        /// <summary>
        /// the bazooka is carried by victoria
        /// </summary>
        Bazooka bazooka = null;

        protected BoundingCylinder boundingCylinder;

        // light data (in two places)
        private double[] lightData =
{   1,      568,      246,    1036,   0.53,   0.53,   0.53,     821,     224, 
  941,  14.2941,       45, 43.9412,    814,    224,   1275,    82.5,       0,  0,
    2,       -5,      169,     428, 0.3964,  0.503, 0.4044,    -5.4,     169,
 1020, 129.4902, 107.5686, 41.8039,   -5.4,    169,   -138, 37.8275,      91, 91,
    3,      113,      217,    -933,    0.5,      0,      0,    -129,     185,
-1085,	     50,        0,       0,    501,    185,  -1087,      48,       0,  0,
    4,      781,      209,    -998,    0.2, 0.1678, 0.1341,    1183,     209,
 -998,	     50,  41.9608, 33.5294,    984,    113,   -932,       0,      80,  0,
    5,      782,      177,    -463,   0.65, 0.5455, 0.4359,     563,     195,
 -197,	     50,        0,       0,   1018,    181,   -188,      80,       0,  0,
    6,     1182,      177,   -1577,   0.65, 0.5455, 0.4359,     971,     181,
-1801,        0,  13.1765,      80,   1406,    181,  -1801,       0, 13.1765,  80};

        public Matrix DeltaMatrix { get { return deltaMatrix; } }
        public Matrix RootMatrix { get { return inverseBindTransforms[skelToBone[0]] * rootMatrixRaw; } }

        public Vector3 SpineOrientation { get { return spineOrientation; } 
            set 
            {
                float x, z;
                if (value.X < minSpineOrientation.X)
                    x = minSpineOrientation.X;
                else if (value.X > maxSpineOrientation.X)
                    x = maxSpineOrientation.X;
                else
                    x = value.X;

                if (value.Z < minSpineOrientation.Z)
                    z = minSpineOrientation.Z;
                else if (value.Z > maxSpineOrientation.Z)
                    z = maxSpineOrientation.Z;
                else
                    z = value.Z;

                spineOrientation = new Vector3(x, value.Y, z);
            } 
        }
        public int SpineBone { get { return spineBone; } set { spineBone = value; } }
        public int HandBone { get { return handBone; } set { handBone = value; } }
        public Bazooka Bazooka { get { return bazooka; } }

        /// <summary>
        /// Access the current animation player
        /// </summary>
        public AnimationPlayer Player { get { return player; } }

        /// <summary>
        /// This class describes a single animation clip we load from
        /// an asset.
        /// </summary>
        private class AssetClip
        {
            public AssetClip(string name, string asset)
            {
                Name = name;
                Asset = asset;
                TheClip = null;
            }

            public string Name { get; set; }
            public string Asset { get; set; }
            public AnimationClips.Clip TheClip { get; set; }
        }

        /// <summary>
        /// A dictionary that allows us to look up animation clips
        /// by name. 
        /// </summary>
        private Dictionary<string, AssetClip> assetClips = new Dictionary<string, AssetClip>();

        public AnimatedModel(PrisonGame game, string asset)
        {
            this.game = game;
            this.asset = asset;
            bazooka = new Bazooka(game);

            skinTransforms = new Matrix[57];
            for (int i = 0; i < skinTransforms.Length; i++)
            {
                skinTransforms[i] = Matrix.Identity;
            }
        }

        /// <summary>
        /// This function is called to load content into this component
        /// of our game.
        /// </summary>
        /// <param name="content">The content manager to load from.</param>
        public virtual void LoadContent(ContentManager content)
        {
            model = content.Load<Model>(asset);

            bazooka.LoadContent(content);

            int boneCnt = model.Bones.Count;
            bindTransforms = new Matrix[boneCnt];
            boneTransforms = new Matrix[boneCnt];
            absoTransforms = new Matrix[boneCnt];

            model.CopyBoneTransformsTo(bindTransforms);
            model.CopyBoneTransformsTo(boneTransforms);
            model.CopyAbsoluteBoneTransformsTo(absoTransforms);

            AnimationClips clips = model.Tag as AnimationClips;
            if (clips != null && clips.SkelToBone.Count > 0)
            {
                skelToBone = clips.SkelToBone;

                inverseBindTransforms = new Matrix[boneCnt];
                skinTransforms = new Matrix[NumSkinBones];

                model.CopyAbsoluteBoneTransformsTo(inverseBindTransforms);

                for (int b = 0; b < inverseBindTransforms.Length; b++)
                    inverseBindTransforms[b] = Matrix.Invert(inverseBindTransforms[b]);

                for (int i = 0; i < skinTransforms.Length; i++)
                    skinTransforms[i] = Matrix.Identity;
            }

            // load the animations
            foreach (AssetClip clip in assetClips.Values)
            {
                Model clipmodel = content.Load<Model>(clip.Asset);
                AnimationClips modelclips = clipmodel.Tag as AnimationClips;
                // the dance animation
                clip.TheClip = modelclips.Clips["Take 001"];
            }
        }

        /// <summary>
        /// This function is called to update this component of our game
        /// to the current game time.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(Double delta)
        {
            if (player != null)
            {
                // Update the clip
                player.Update(delta);

                for (int b = 0; b < player.BoneCount; b++)
                {
                    AnimationPlayer.Bone bone = player.GetBone(b);
                    if (!bone.Valid)
                        continue;

                    Vector3 scale = new Vector3(bindTransforms[b].Right.Length(),
                        bindTransforms[b].Up.Length(),
                        bindTransforms[b].Backward.Length());

                    boneTransforms[b] = Matrix.CreateScale(scale) *
                        Matrix.CreateFromQuaternion(bone.Rotation) *
                        Matrix.CreateTranslation(bone.Translation);
                }

                model.CopyBoneTransformsFrom(boneTransforms);
            }

            if (skelToBone != null)
            {
                int rootBone = skelToBone[0];

                deltaMatrix = Matrix.Invert(rootMatrixRaw) * boneTransforms[rootBone];
                DeltaPosition = boneTransforms[rootBone].Translation - rootMatrixRaw.Translation;

                rootMatrixRaw = boneTransforms[rootBone];

                boneTransforms[rootBone] = bindTransforms[rootBone];
            }

            boneTransforms[spineBone] = Matrix.CreateRotationX(spineOrientation.X) *
                Matrix.CreateRotationZ(spineOrientation.Z) * bindTransforms[spineBone];

            model.CopyBoneTransformsFrom(boneTransforms);
            model.CopyAbsoluteBoneTransformsTo(absoTransforms);

            // move the head (and the ponytails with it)
            //boneTransforms[9] = Matrix.CreateRotationX(0.8f) * bindTransforms[9];

            //
            // Bazooka
            //

            bazooka.Transform =
                Matrix.CreateRotationX(MathHelper.ToRadians(109.5f)) *
                Matrix.CreateRotationY(MathHelper.ToRadians(9.7f))*
                Matrix.CreateRotationZ(MathHelper.ToRadians(72.9f)) *
                Matrix.CreateTranslation(-9.6f, 11.85f, 21.1f) *
                absoTransforms[handBone] *
                game.Player.Transform;
        }

        public int GetBoneIndex(String boneName)
        {
            return model.Bones.IndexOf(model.Bones[boneName]);
        }

        public void RotateBone(int boneIndex, float angle)
        {
            skinTransforms[boneIndex] = Matrix.CreateRotationY(angle) * skinTransforms[boneIndex];
        }

        /// <summary>
        /// Add an asset clip to the dictionary.
        /// </summary>
        /// <param name="name">Name we will use for the clip</param>
        /// <param name="asset">The FBX asset to load</param>
        public void AddAssetClip(string name, string asset)
        {
            assetClips[name] = new AssetClip(name, asset);
        }

        /// <summary>
        /// Play an animation clip on this model.
        /// </summary>
        /// <param name="name"></param>
        public AnimationPlayer PlayClip(string name)
        {
            player = null;

            if (name != "Take 001")
            {
                player = new AnimationPlayer(this, assetClips[name].TheClip);
                Update(0);
                return player;
            }

            AnimationClips clips = model.Tag as AnimationClips;
            if (clips != null)
            {
                player = new AnimationPlayer(this, clips.Clips[name]);
                Update(0);
            }

            return player;
        }

        public virtual bool TestSphereForCollision(BoundingSphere bs, Vector3 myLocation)
        {
            if (boundingCylinder == null)
            {
                int rootBone = skelToBone[0];
                boundingCylinder = new BoundingCylinder(200f, 50f, myLocation);
            }
            boundingCylinder.Location = myLocation;

            return boundingCylinder.Intersect(bs);
        }

        /// <summary>
        /// This function is called to draw this game component.
        /// </summary>
        /// <param name="graphics">Device to draw the model on.</param>
        /// <param name="gameTime">Current game time.</param>
        /// <param name="transform">Transform that puts the model where we want it.</param>
        public virtual void Draw(GraphicsDeviceManager graphics, GameTime gameTime, Matrix transform)
        {
            bazooka.Draw(graphics, gameTime);
            DrawModel(graphics, model, transform);
        }

        private void DrawModel(GraphicsDeviceManager graphics, Model model, Matrix world)
        {
            // boneTransforms[6] = Matrix.CreateRotationY(0.8f); //<-- changes the head to rotate (without ponytails)
            
            if (skelToBone != null)
            {
                for (int b = 0; b < skelToBone.Count; b++)
                {
                    int n = skelToBone[b];
                    skinTransforms[b] = inverseBindTransforms[n] * absoTransforms[n];
                }
            }

            

            foreach (ModelMesh mesh in model.Meshes)
            {
                // changed from BasicEffect to Effect --> now only works for skinned models. Will have to change
                // later to accomodate a different effect
                foreach (Effect effect in mesh.Effects)
                {
                    effect.Parameters["World"].SetValue(absoTransforms[mesh.ParentBone.Index] * world);

                    effect.Parameters["View"].SetValue(game.Camera.View);
                    effect.Parameters["Projection"].SetValue(game.Camera.Projection);

                    effect.Parameters["Light1Location"].SetValue(LightInfo(game.Player.Section, 0));
                    effect.Parameters["Light1Color"].SetValue(LightInfo(game.Player.Section, 1));
                    effect.Parameters["Light2Location"].SetValue(LightInfo(game.Player.Section, 2));
                    effect.Parameters["Light2Color"].SetValue(LightInfo(game.Player.Section, 3));
                    effect.Parameters["Light3Location"].SetValue(LightInfo(game.Player.Section, 4));
                    effect.Parameters["Light3Color"].SetValue(LightInfo(game.Player.Section, 5));
                    effect.Parameters["Light4Location"].SetValue(LightInfo(game.Player.OtherSection, 0));
                    effect.Parameters["Light4Color"].SetValue(LightInfo(game.Player.OtherSection, 1));
                    effect.Parameters["Gain"].SetValue(game.Player.Gain);
                    effect.Parameters["Slime"].SetValue(game.SlimeLevel);
                    effect.Parameters["Bones"].SetValue(skinTransforms);
                }
                mesh.Draw();
            }

        }

        /// <summary>
        /// for getting the correct information about lights
        /// </summary>
        /// <param name="section"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        protected Vector3 LightInfo(int section, int item)
        {
            int offset = (section - 1) * 19 + 1 + (item * 3);
            return new Vector3((float)lightData[offset],
                               (float)lightData[offset + 1],
                               (float)lightData[offset + 2]);
        }
    }
}
