﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace PrisonStep
{
    class AlienAnimatedModel : AnimatedModel
    {
        /// <summary>
        /// the velocity
        /// </summary>
        private float velocity = 3f;

        /// <summary>
        /// the angle of the head
        /// </summary>
        private float headAngle = 0f;

        /// <summary>
        /// the offset of the headangle
        /// </summary>
        private const float headAngleOffset = -0.4f;

        /// <summary>
        /// current position
        /// </summary>
        private Vector3 position = Vector3.Zero;

        /// <summary>
        /// the orientation
        /// </summary>
        float orientation = 0;

        /// <summary>
        /// transform
        /// </summary>
        private Matrix transform = Matrix.Identity;

        #region Properties
        /// <summary>
        /// the position
        /// </summary>
        public Vector3 Position { get { return position; } set { position = value; } }

        /// <summary>
        /// the orientation
        /// </summary>
        public float Orientation { get { return orientation; } set { orientation = value; } }

        /// <summary>
        /// the model of the pies
        /// </summary>
        public Model Model { get { return model; } set { model = value; } }

        /// <summary>
        /// Alien's velocity
        /// </summary>
        public float Velocity { get { return velocity; } set { velocity = value; } }

        #endregion

        public AlienAnimatedModel(PrisonGame game, string asset)
            : base(game, asset)
        {
            // nothing
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            // set the index of the head
            //head = model.Bones.IndexOf(model.Bones["Head"]);
            //plungerArm = model.Bones.IndexOf(model.Bones["PlungerArm"]);
            //arm2 = model.Bones.IndexOf(model.Bones["Arm2"]);
        }

        public void SetAlienHeadRotation(Vector3 playerLocation)
        {
            Vector3 direction = playerLocation - position;
            headAngle = (float)Math.Atan2((double)direction.X, (double)direction.Z);
        }

        /// <summary>
        /// Set the value of transform to match the current location
        /// and orientation.
        /// </summary>
        private void SetAlienTransform()
        {
            transform = Matrix.CreateRotationY(orientation);
            transform.Translation += position;
        }

        public override void Update(Double delta)
        {
            if (player != null)
            {
                // Update the clip
                player.Update(delta);

                for (int b = 0; b < player.BoneCount; b++)
                {
                    AnimationPlayer.Bone bone = player.GetBone(b);
                    if (!bone.Valid)
                        continue;

                    Vector3 scale = new Vector3(bindTransforms[b].Right.Length(),
                        bindTransforms[b].Up.Length(),
                        bindTransforms[b].Backward.Length());

                    boneTransforms[b] = Matrix.CreateScale(scale) *
                        Matrix.CreateFromQuaternion(bone.Rotation) *
                        Matrix.CreateTranslation(bone.Translation);
                }

                model.CopyBoneTransformsFrom(boneTransforms);
            }

            if (skelToBone != null)
            {
                int rootBone = skelToBone[0];

                deltaMatrix = Matrix.Invert(rootMatrixRaw) * boneTransforms[rootBone];
                DeltaPosition = boneTransforms[rootBone].Translation - rootMatrixRaw.Translation;

                rootMatrixRaw = boneTransforms[rootBone];

                boneTransforms[rootBone] = bindTransforms[rootBone];
            }

            model.CopyBoneTransformsFrom(boneTransforms);
            model.CopyAbsoluteBoneTransformsTo(absoTransforms);
        }

        public override bool TestSphereForCollision(BoundingSphere bs, Vector3 myLocation)
        {
            if (boundingCylinder == null)
            {
                boundingCylinder = new BoundingCylinder(200f, 60f, myLocation);
            }
            boundingCylinder.Location = myLocation;

            return boundingCylinder.Intersect(bs);
        }

        /// <summary>
        /// This function is called to draw this game component.
        /// </summary>
        /// <param name="graphics">Device to draw the model on.</param>
        /// <param name="gameTime">Current game time.</param>
        /// <param name="transform">Transform that puts the model where we want it.</param>
        public override void Draw(GraphicsDeviceManager graphics, GameTime gameTime, Matrix transform)
        {
            DrawModel(graphics, model, transform);
        }

        private void DrawModel(GraphicsDeviceManager graphics, Model model, Matrix world)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            // TODO - look at animation model for how to move head --> should help with moving victoria's spine too
            int count = model.Bones.Count;
            for (int i = 0; i < count; i++)
            {
                //if (i == head || i == plungerArm || i == arm2)
                //    transforms[i] = Matrix.CreateRotationZ(headAngle + headAngleOffset);
                //else
                    transforms[i] = Matrix.Identity;

                ModelBone bone = model.Bones[i];
                if (bone.Parent == null)
                    transforms[i] *= bone.Transform;
                else
                    transforms[i] *= bone.Transform * transforms[bone.Parent.Index];
            }
            //

            if (skelToBone != null)
            {
                for (int b = 0; b < skelToBone.Count; b++)
                {
                    int n = skelToBone[b];
                    skinTransforms[b] = inverseBindTransforms[n] * absoTransforms[n]/* * Matrix.CreateRotationY(spineOrientation)*/;
                }
            }

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (Effect effect in mesh.Effects)
                {
                    effect.Parameters["World"].SetValue(absoTransforms[mesh.ParentBone.Index] * world);

                    effect.Parameters["View"].SetValue(game.Camera.View);
                    effect.Parameters["Projection"].SetValue(game.Camera.Projection);

                    effect.Parameters["World"].SetValue(absoTransforms[mesh.ParentBone.Index] * world);
                    effect.Parameters["View"].SetValue(game.Camera.View);
                    effect.Parameters["Projection"].SetValue(game.Camera.Projection);

                    effect.Parameters["Light1Location"].SetValue(LightInfo(game.Player.Section, 0));
                    effect.Parameters["Light1Color"].SetValue(LightInfo(game.Player.Section, 1));
                    effect.Parameters["Light2Location"].SetValue(LightInfo(game.Player.Section, 2));
                    effect.Parameters["Light2Color"].SetValue(LightInfo(game.Player.Section, 3));
                    effect.Parameters["Light3Location"].SetValue(LightInfo(game.Player.Section, 4));
                    effect.Parameters["Light3Color"].SetValue(LightInfo(game.Player.Section, 5));
                    effect.Parameters["Light4Location"].SetValue(LightInfo(game.Player.OtherSection, 0));
                    effect.Parameters["Light4Color"].SetValue(LightInfo(game.Player.OtherSection, 1));
                    effect.Parameters["Gain"].SetValue(game.Player.Gain);

                    effect.Parameters["Bones"].SetValue(skinTransforms);
                }
                mesh.Draw();
            }
        }
    }
}
