
float4x4 World;
float4x4 View;
float4x4 Projection;

#include "Lights.fx"

float Slime = 1;

// Maximum number of bone matrices we can render using shader 2.0 in a single pass.
// If you change this, update SkinnedModelProcessor.cs to match.
#define MaxBones 57
float4x4 Bones[MaxBones];

texture Texture;

sampler Sampler = sampler_state
{
    Texture = <Texture>;

    MinFilter = LINEAR;
    MagFilter = LINEAR;
    
    AddressU = Wrap;
    AddressV = Wrap;
    AddressW = Wrap;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
	float4 BoneIndices : BLENDINDICES0;
    float4 BoneWeights : BLENDWEIGHT0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord : TEXCOORD0;
	float4 Pos1 : TEXCOORD1;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	// Blend between the weighted bone matrices.
    float4x4 skinTransform = 0;
    
    skinTransform += Bones[input.BoneIndices.x] * input.BoneWeights.x;
    skinTransform += Bones[input.BoneIndices.y] * input.BoneWeights.y;
    skinTransform += Bones[input.BoneIndices.z] * input.BoneWeights.z;
    skinTransform += Bones[input.BoneIndices.w] * input.BoneWeights.w;

    VertexShaderOutput output;
	output.TexCoord = input.TexCoord;

	float4 worldPosition = mul(mul(input.Position, skinTransform), World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	
    output.Color = ComputeColor(worldPosition, input.Normal);
	output.Pos1 = output.Position;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    // Compute a value that ranges from -1 to 1, where -1 is the bottom of 
	// the screen and 1 is the top.
	float y = input.Pos1.y / input.Pos1.w;   
	
	// (y - Slime) > 0 above the slime line. The * 5 makes the line faded rather
	// than a hard line.  
	float sy = saturate((y - Slime) * 5);

	// Compute the slime color
	float slime = sy * float4(0.4, 1.0, 0.4, 1) + (1 - sy) * float4(1, 1, 1, 1);

	// Output color multiplied by the slime color
    return input.Color * tex2D(Sampler, input.TexCoord) * slime;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
