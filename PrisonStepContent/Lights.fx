float3 DiffuseColor;

float3 LightAmbient = float3(0.05, 0.05, 0.10);
float3 Light1Location = float3(568, 246, 1036);
float3 Light1Color = float3(1, 1, 1);
float3 Light2Location = float3(821, 224, 941);
float3 Light2Color = float3(14.29, 45, 43.94);
float3 Light3Location = float3(824, 231, 765);
float3 Light3Color = float3(82.5, 0, 0);
float3 Light4Location = float3(824, 231, 765);
float3 Light4Color = float3(0, 0, 0);

float3 Gain = float3(1, 1, 1);

// worldPosition - The vertex position in the world
// vNormal - The normal in the model
float4 ComputeColor(float4 worldPosition, float3 vNormal)
{
    float3 color = LightAmbient;

    float3 normal = normalize(mul(vNormal, World));

    float3 L1 = normalize(Light1Location - worldPosition);
    color += saturate(dot(L1, normal)) * Light1Color;
    
    float3 L2 = Light2Location - worldPosition;
    float L2distance = length(L2);
    L2 /= L2distance;
    color += saturate(dot(L2, normal)) / L2distance * Light2Color;

    float3 L3 = Light3Location - worldPosition;
    float L3distance = length(L3);
    L3 /= L3distance;
    color += saturate(dot(L3, normal)) / L3distance * Light3Color;

	float3 L4 = normalize(Light4Location - worldPosition);
    color += saturate(dot(L4, normal)) * Light4Color * Gain;

    return float4(color, 1);
}

/*
// TODO: add effect parameters here.

struct VertexShaderInput
{
    float4 Position : POSITION0;

    // TODO: add input channels such as texture
    // coordinates and vertex colors here.
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;

    // TODO: add vertex shader outputs such as colors and texture
    // coordinates here. These values will automatically be interpolated
    // over the triangle, and provided as input to your pixel shader.
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

    // TODO: add your vertex shader code here.

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    // TODO: add your pixel shader code here.

    return float4(1, 0, 0, 1);
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}

*/